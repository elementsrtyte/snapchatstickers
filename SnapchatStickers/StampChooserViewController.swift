//
//  StampChooserViewController.swift
//  SnapchatStickers
//
//  Created by Neil Bhargava on 12/13/16.
//  Copyright © 2016 Blueprint. All rights reserved.
//

import UIKit
import BPGridCollectionViewFlowLayout

protocol StampChooserViewControllerDelegate {
    func didSelectStamp(image: UIImage)
}

enum Stickers {
    case general
    case foodDrink
    case outdoorsRecreation
    case ratings
    case reactions
    case letters
    case ranking
    
    func prettyText()->String {
        switch self {
        case .general:
            return "General"
        case .foodDrink:
            return "Food & Drink"
        case .outdoorsRecreation:
            return "Outdoors & Recreation"
        case .ratings:
            return "Ratings"
        case .reactions:
            return "Reactions"
        case .letters:
            return "Letters"
        case .ranking:
            return "Ranking"
            
        }
        
    }
    
    func imageName()->String {
        switch self {
        case .general:
            return "generalStamp"
        case .foodDrink:
            return "foodStamp"
        case .outdoorsRecreation:
            return "outdoorsStamp"
        case .ratings:
            return "ratingsStamp"
        case .reactions:
            return "reactionsStamp"
        case .letters:
            return "lettersStamp"
        case .ranking:
            return "rankingsStamp"
        }
        
    }
}

class StampChooserViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    let collectionView: UICollectionView
    
    var delegate: StampChooserViewControllerDelegate
  
    let headers: [Stickers] = [.general, .foodDrink, .outdoorsRecreation, .ratings, .reactions, .letters, .ranking]
    let data: [[UIImage]]
    
    init(delegate: StampChooserViewControllerDelegate){
        let collectionViewLayout = BPGridCollectionViewFlowLayout (numberOfColumns: 4)
      
        var tmpArray: [[UIImage]] = []
        for header in headers {
            var imgs: [UIImage] = []
            for i in 1..<Int.max {
                guard let img = UIImage(named: "\(header.imageName())\(i)") else {
                    tmpArray.append(imgs)
                    break
                }
                imgs.append(img)
            }
        }
        self.data = tmpArray
        
        
        collectionViewLayout.headerReferenceSize = CGSize(width: 0, height: 70);
        
        self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: collectionViewLayout)
        self.delegate = delegate
        
        super.init(nibName: nil, bundle: nil)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubviewFull(view: self.collectionView)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.collectionView.register(StampCell.self, forCellWithReuseIdentifier: "cell")
        
        
        self.collectionView.register(SectionHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header")
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header", for: indexPath) as! SectionHeaderView
        header.setup(title: self.headers[indexPath.section].prettyText())
        return header
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! StampCell
        cell.setup(image: self.data[indexPath.section][indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.dismiss(animated: true) {
            self.delegate.didSelectStamp(image: self.data[indexPath.section][indexPath.row])
        }
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.data.count
    }
    
    
}

class StampCell: UICollectionViewCell {
    let imageView = UIImageView(frame: CGRect.zero)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentView.addSubviewFull(view: self.imageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(image: UIImage){
        self.imageView.image = image
    }
    
}

class SectionHeaderView: UICollectionReusableView {

    let label = BPLabelInset(frame: CGRect.zero)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.black
        self.addSubviewFull(view: label)
        self.label.font = UIFont.boldSystemFont(ofSize: 30)
        self.label.textColor = UIColor(red: 204.0 / 255.0, green: 153.0 / 255.0, blue: 0.0, alpha: 1.0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(title: String){
        self.label.text = title
    }
}

class BPLabelInset: UILabel {
    
    let inset: CGFloat
    
    init(frame: CGRect = CGRect.zero, inset: CGFloat = 10) {
        self.inset = inset
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.inset = 10
        super.init(coder: aDecoder)
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: 0, left: inset, bottom: 0, right: inset)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
}
