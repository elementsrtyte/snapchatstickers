//
//  UIViewExtensions.swift
//  SnapchatStickers
//
//  Created by Neil Bhargava on 12/15/16.
//  Copyright © 2016 Blueprint. All rights reserved.
//

import UIKit

extension UIView {

    func addSubviewFull(view: UIView){
        let constraintDict = ["view":view]
        view.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(view)
        var constraints = NSLayoutConstraint.constraints(withVisualFormat: "|[view]|", options: NSLayoutFormatOptions.alignAllLeft, metrics: nil, views: constraintDict)
        constraints.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: NSLayoutFormatOptions.alignAllTop, metrics: nil, views: constraintDict))
        self.addConstraints(constraints)
    }

    
    func allSubviews()->[UIView] {
        var arr: [UIView] = []
        arr.append(self)
        for subview in self.subviews {
            arr.append(contentsOf: subview.allSubviews())
        }
        return arr
    }
    
    func takeScreenshot(hiddenElements: [UIView] = [])->UIImage?{
        for element in hiddenElements {
            element.isHidden = true
        }
        
        UIGraphicsBeginImageContext(self.frame.size)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        for element in hiddenElements {
            element.isHidden = false
        }
        
        return image
    }
}
