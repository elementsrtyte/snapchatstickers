
//
//  ScreenshotViewController.swift
//  SnapchatStickers
//
//  Created by Neil Bhargava on 12/15/16.
//  Copyright © 2016 Blueprint. All rights reserved.
//

import UIKit

class ScreenshotViewController: UIViewController {

    var image: UIImage?
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageView.image = image

        // Do any additional setup after loading the view.
    }

}
