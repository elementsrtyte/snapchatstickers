//
//  ViewController.swift
//  SnapchatStickers
//
//  Created by Neil Bhargava on 12/13/16.
//  Copyright © 2016 Blueprint. All rights reserved.
//

import UIKit
import BPGridCollectionViewFlowLayout

class ViewController: UIViewController, StampChooserViewControllerDelegate, UIGestureRecognizerDelegate {
    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var addStampButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func buttonTapped(_ sender: Any) {
        let vc = StampChooserViewController(delegate: self)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func saveButtonTapped(_ sender: Any) {
    }
    
    
    func didSelectStamp(image: UIImage) {
        
        print("delegate called successfully")
        let stampSize: CGFloat = 100
        let stampCenter = CGPoint(x: self.view.center.x - stampSize/2, y: self.view.center.y - stampSize/2)
        let stampView = UIImageView(frame: CGRect(origin: stampCenter, size: CGSize(width: stampSize, height: stampSize)))
        stampView.image = image
        stampView.isUserInteractionEnabled = true
        self.view.addSubview(stampView)
        let pan = UIPanGestureRecognizer(target: self, action: #selector(ViewController.handlePan(recognizer:)))
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(ViewController.handlePinch(recognizer:)))
        let rotate  = UIRotationGestureRecognizer(target: self, action: #selector(ViewController.handleRotate(recognizer:)))

        pan.delegate = self
        pinch.delegate = self
        rotate.delegate = self
        
        stampView.addGestureRecognizer(pan)
        stampView.addGestureRecognizer(pinch)
        stampView.addGestureRecognizer(rotate)
    }
    
    //MARK: Misc. Gesture Recognizers
    func handlePan(recognizer:UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: self.view)
        if let view = recognizer.view {
            view.center = CGPoint(x:view.center.x + translation.x,
                                  y:view.center.y + translation.y)
        }
        recognizer.setTranslation(CGPoint.zero, in: self.view)
    }
    
    func handlePinch(recognizer : UIPinchGestureRecognizer) {
        if let view = recognizer.view {
            view.transform = view.transform.scaledBy(x: recognizer.scale, y: recognizer.scale)
            recognizer.scale = 1
        }
    }
    
    func handleRotate(recognizer : UIRotationGestureRecognizer) {
        if let view = recognizer.view {
            view.transform = view.transform.rotated(by: recognizer.rotation)
            recognizer.rotation = 0
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! ScreenshotViewController
        vc.image = self.view.takeScreenshot(hiddenElements: [self.addStampButton, self.saveButton])
    }
    

}
